package core;

public class KopiLuwakCoffee extends Coffee {
	private final String DESCRIPTION;

	public KopiLuwakCoffee() {
		super(80);
		DESCRIPTION = "Kopi Luwak Coffee";
	}

	@Override
	public String getDescription() { return DESCRIPTION; }
}
