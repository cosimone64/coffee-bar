package core;

public class MilkDecorator extends CoffeeDecorator {
	private final String STRING_REPRESENTATION;
	private final String DESCRIPTION;
	private final double PRICE;

	public MilkDecorator(Coffee c) {
		super(c);
		STRING_REPRESENTATION = "Milk";
		DESCRIPTION = "with milk";
		PRICE = 1.0;
	}

	@Override
	public double getPrice() {
		return super.getPrice() + getMilkPrice();
	}

	@Override
	public String getDescription() {
		return super.getDescription() + getMilkDesc();
	}

	@Override
	public String toString() { return STRING_REPRESENTATION; }

	@Override
	public Coffee createCoffeeDec(Coffee c) {
		return new MilkDecorator(c);
	}

	public double getMilkPrice() { return PRICE; }

	public String getMilkDesc() { return DESCRIPTION; }
}
