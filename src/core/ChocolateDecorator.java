package core;

public class ChocolateDecorator extends CoffeeDecorator {
	private double CHOCOLATE_PRICE;
	private String STRING_REPRESENTATION;
	private String DESCRIPTION;

	public ChocolateDecorator(Coffee c) {
		super(c);
		CHOCOLATE_PRICE = 1.5;
		STRING_REPRESENTATION = "Chocolate";
		DESCRIPTION = "with chocolate";
	}

	@Override
	public double getPrice() {
		return super.getPrice() + getChocolatePrice();
	}

	@Override
	public String getDescription() {
		return super.getDescription() + getChocolateDesc();
	}

	@Override
	public String toString() { return STRING_REPRESENTATION; }

	@Override
	public Coffee createCoffeeDec(Coffee c) {
		return new ChocolateDecorator(c);
	}

	public double getChocolatePrice() { return CHOCOLATE_PRICE; }

	public String getChocolateDesc() { return DESCRIPTION; }
}
