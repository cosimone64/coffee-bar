package core;

public abstract class CocktailDecorator extends Cocktail {
	private Cocktail cocktailToBeDecorated;

	public CocktailDecorator(Cocktail c) {
		super(c.getPrice());
		cocktailToBeDecorated = c;
	}

	public String getDescriprtion() {
		return cocktailToBeDecorated.getDescription();
	}

	public abstract Cocktail createCocktailDec(Cocktail c);

	public abstract String toString();
}
