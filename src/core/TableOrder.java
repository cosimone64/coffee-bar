package core;

import java.util.Iterator;
import java.util.LinkedList;

public class TableOrder implements Order {
	private LinkedList<Order> orderList;
	private Invoice tableInvoice;
	private double totalPrice;

	public TableOrder() {
		orderList = new LinkedList<Order>();
		totalPrice = 0;
	}

	@Override
	public double getPrice() {
		double tot = 0;
		Iterator<Order> see = getIterator();
		while (see.hasNext()) {
			Order tmp = see.next();
			tot = tot + tmp.getPrice();
		}
		return tot;
	}

	@Override
	public String getDescription() {
		return tableInvoice.printInvoice(this);
	}

	@Override
	public void addOrder(Order o) {
		orderList.add(o);
		increasePrice(o);
	}

	@Override
	public void deleteOrder(int i) {
		Order tmp = orderList.get(i);
		decreasePrice(tmp);
		orderList.remove(i);
	}

	@Override
	public void setInvoice(Invoice i) { tableInvoice = i; }

	public Iterator<Order> getIterator() { return orderList.iterator(); }

	private void increasePrice(Order o) {
		totalPrice = totalPrice + o.getPrice();
	}

	private void decreasePrice(Order o){
		totalPrice = totalPrice - o.getPrice();
	}
}
