package core;

public class FruitCocktail extends Cocktail {
	private final String DESCRIPTION;

	public FruitCocktail() {
		super(4);
		DESCRIPTION = "Fruit Cocktail";
	}

	@Override
	public String getDescription() { return DESCRIPTION; }
}
