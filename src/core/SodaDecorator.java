package core;

public class SodaDecorator extends CocktailDecorator {
	private final String STRING_REPRESENTATION;
	private final String DESCRIPTION;
	private final double PRICE;

	public SodaDecorator(Cocktail c) {
		super(c);
		STRING_REPRESENTATION = "Soda";
		DESCRIPTION = "with soda";
		PRICE = 2.5;
	}
	
	@Override
	public double getPrice() {
		return super.getPrice() + getSodaPrice();
	}

	@Override
	public String getDescription() {
		return super.getDescriprtion() + getSodaDesc();
	}

	@Override
	public String toString() { return STRING_REPRESENTATION; }

	@Override
	public Cocktail createCocktailDec(Cocktail c) {
		return new SodaDecorator(c);
	}

	public double getSodaPrice() { return PRICE; }

	public String getSodaDesc() {return DESCRIPTION; }
}
