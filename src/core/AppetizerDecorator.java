package core;

public class AppetizerDecorator extends CocktailDecorator {
	private final int APPETIZER_PRICE;
	private final String APPETIZER_DESCRIPTION;

	public AppetizerDecorator(Cocktail c) {
		super(c);
		APPETIZER_PRICE = 5;
		APPETIZER_DESCRIPTION = "with an appetizer";
	}

	@Override
	public double getPrice() {
		return super.getPrice() + getAppetizerPrice();
	}

	@Override
	public String getDescription() {
		return super.getDescriprtion() + getAppetizerDesc();
	}

	@Override
	public String toString() { return "Appetizer"; }

	@Override
	public Cocktail createCocktailDec(Cocktail c) {
		return new AppetizerDecorator(c);
	}

	public int getAppetizerPrice() { return APPETIZER_PRICE; }

	public String getAppetizerDesc() { return APPETIZER_DESCRIPTION; }
}
