package core;

public class Martini extends Cocktail {
	private final String DESCRIPTION;

	public Martini() {
		super(6);
		DESCRIPTION = "Martini";
	}

	@Override
	public String getDescription() { return DESCRIPTION; }
}
