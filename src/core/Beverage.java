package core;

import exception.*;

public abstract class Beverage implements Order {
	private double value;

	public Beverage(double val) { value = val; }

	@Override
	public double getPrice() { return value; }

	@Override
	public void addOrder(Order obj) throws InvalidOperationException {
		throw new InvalidOperationException();
	}

	@Override
	public void deleteOrder(int i) throws InvaldDeleteException {
		throw new InvaldDeleteException();
	}

	@Override
	public void setInvoice(Invoice i) throws InvalidInvoiceException {
		throw new InvalidInvoiceException();
	}

	public String toString() { return getDescription(); }

	public abstract String getDescription();
}
