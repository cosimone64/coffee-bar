package core;

public class ArabicCoffee extends Coffee {
	private final String DESCRIPTION;

	public ArabicCoffee() {
		super(2);
		DESCRIPTION = "Arabic Coffee";
	}

	@Override
	public String getDescription() { return DESCRIPTION; }
}
