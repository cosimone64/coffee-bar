package core;

import exception.*;

public interface Order {
	public double getPrice();

	public String getDescription();

	public void addOrder(Order obj) throws InvalidOperationException;

	public void deleteOrder(int i) throws InvaldDeleteException;

	public void setInvoice(Invoice i) throws InvalidInvoiceException;
}
