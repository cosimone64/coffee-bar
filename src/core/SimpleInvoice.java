package core;

import java.util.Iterator;

public class SimpleInvoice implements Invoice {
	@Override
	public String printInvoice(TableOrder t) {
		String tot = "";
		Iterator<Order> see = t.getIterator();

		while (see.hasNext()) {
			Order tmp = see.next();
			tot = tot + tmp.getDescription() + "\n";
			tot = tot + tmp.getPrice() + "€\n";
		}
		return tot;
	}
}
