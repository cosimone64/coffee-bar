package core;

public abstract class CoffeeDecorator extends Coffee {
	private Coffee coffeeToBeDecorated;

	public CoffeeDecorator(Coffee c) {
		super(c.getPrice());
		coffeeToBeDecorated = c;
	}

	public String getDescription() {
		return coffeeToBeDecorated.getDescription();
	}

	public abstract Coffee createCoffeeDec(Coffee c);

	public abstract String toString();
}
