package gui;

import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.border.EmptyBorder;
import core.*;

public abstract class Tablet extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;

	protected JComboBox<Coffee> coffeeBox;
	protected JComboBox<Cocktail> cocktailBox;
	protected JComboBox<CoffeeDecorator> coffeeDecoratorBox;
	protected JComboBox<CocktailDecorator> cocktailDecoratorBox;
	protected JTextPane currentOrderPane;
	protected JTextPane invTextPane;
	protected JTextPane pricePane;
	protected JButton orderButton;
	protected Coffee[] coffeeModel;
	protected CoffeeDecorator[] coffeeDecModel;
	protected Cocktail[] cocktailModel;
	protected CocktailDecorator[] cocktailDecModel;
	protected Beverage selectedBeverage;
	protected TableOrder table;
	private JPanel contentPane;
	private JLabel coffeeLabel;
	private JLabel cocktailLabel;
	private JLabel decorationLabel;
	private JLabel currentOrderLabel;
	private JLabel invoiceLabel;
	private JLabel priceLabel;
	private JScrollPane invoiceScroll;
	private JScrollPane orderScroll;

	public Tablet() {
		setTableProperties();
		setPanel();
		setFrameProperties();
		initializeModelArrays();
		initializeBoxes();
		setOrderButton();
		initializeLabels();
		setTextPanes();
	}

	private void setTextPanes() {
		setOrderTextPane();
		setInvTextPane();
		setInvoiceScroll();
		setOrderScroll();
		setPricePane();
	}

	private void initializeLabels() {
		setCoffeeLabel();
		setCocktailLabel();
		setDecoratorLabel();
		setOrderLabel();
		setInvoiceLabel();
		setPriceLabel();
	}

	private void initializeBoxes() {
		setCoffeeBox();
		setCocktailBox();
		setCoffeeDecoratorBox();
		setCocktailDecoratorBox();
	}

	private void initializeModelArrays() {
		initializeCoffeeArray();
		initializeCoffeeDecArray();
		initializeCocktailArray();
		initializeCocktailDecArray();
	}

	private void setPanel() {
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLUE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
	}

	private void setTableProperties() {
		table = new TableOrder();
		table.setInvoice(new SimpleInvoice());
	}

	private void setFrameProperties() {
		setTitle("Coffee Bar");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		setLocationRelativeTo(null);
		setResizable(false);
		setContentPane(contentPane);
	}

	private void setCoffeeBox() {
		coffeeBox = new JComboBox<Coffee>();
		coffeeBox.setModel(new DefaultComboBoxModel<Coffee>(coffeeModel));
		coffeeBox.setBounds(20, 40, 200, 30);
		contentPane.add(coffeeBox);
	}

	private void setCocktailBox() {
		cocktailBox = new JComboBox<Cocktail>();
		cocktailBox.setModel(new DefaultComboBoxModel<Cocktail>(cocktailModel));
		cocktailBox.setBounds(293, 39, 200, 30);
		contentPane.add(cocktailBox);
	}

	private void setCoffeeDecoratorBox() {
		coffeeDecoratorBox = new JComboBox<CoffeeDecorator>();
		coffeeDecoratorBox.setBounds(586, 40, 200, 30);
		contentPane.add(coffeeDecoratorBox);
		coffeeDecoratorBox.setModel(
		        new DefaultComboBoxModel<CoffeeDecorator>(coffeeDecModel));
		coffeeDecoratorBox.setVisible(false);
	}

	private void setCocktailDecoratorBox() {
		cocktailDecoratorBox = new JComboBox<CocktailDecorator>();
		cocktailDecoratorBox.setBounds(586, 40, 200, 30);
		contentPane.add(cocktailDecoratorBox);
		cocktailDecoratorBox.setModel(
		        new DefaultComboBoxModel<CocktailDecorator>(cocktailDecModel));
		cocktailDecoratorBox.setVisible(false);
	}

	private void setOrderButton() {
		orderButton = new JButton("Add Order");
		orderButton.setBounds(293, 93, 200, 25);
		contentPane.add(orderButton);
	}

	private void setCoffeeLabel() {
		coffeeLabel = new JLabel("Coffees");
		coffeeLabel.setForeground(Color.ORANGE);
		coffeeLabel.setFont(new Font("Comic Sans MS", Font.BOLD, 20));
		coffeeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		coffeeLabel.setBounds(10, 10, 130, 20);
		contentPane.add(coffeeLabel);
	}

	private void setCocktailLabel() {
		cocktailLabel = new JLabel("Cocktails");
		cocktailLabel.setForeground(Color.ORANGE);
		cocktailLabel.setFont(new Font("Comic Sans MS", Font.BOLD, 22));
		cocktailLabel.setBounds(345, 10, 130, 20);
		contentPane.add(cocktailLabel);
	}

	private void setDecoratorLabel() {
		decorationLabel = new JLabel("Decorations");
		decorationLabel.setForeground(Color.ORANGE);
		decorationLabel.setFont(new Font("Comic Sans MS", Font.BOLD, 22));
		decorationLabel.setHorizontalAlignment(SwingConstants.CENTER);
		decorationLabel.setBounds(621, 10, 130, 20);
		contentPane.add(decorationLabel);
	}

	private void setOrderLabel() {
		currentOrderLabel = new JLabel("Current Order:");
		currentOrderLabel.setForeground(Color.ORANGE);
		currentOrderLabel.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		currentOrderLabel.setBounds(170, 131, 105, 25);
		contentPane.add(currentOrderLabel);
	}

	private void setOrderTextPane() {
		currentOrderPane = new JTextPane();
		currentOrderPane.setEditable(false);
		currentOrderPane.setFont(new Font("Comic Sans MS", Font.PLAIN, 22));
		currentOrderPane.setBounds(293, 130, 493, 60);
		currentOrderPane.setText("");
		contentPane.add(currentOrderPane);
	}

	private void setInvTextPane() {
		invTextPane = new JTextPane();
		invTextPane.setEditable(false);
		invTextPane.setFont(new Font("Comic Sans MS", Font.PLAIN, 20));
		invTextPane.setBounds(0, 0, 0, 0);
		contentPane.add(invTextPane);
	}

	private void setPricePane() {
		pricePane = new JTextPane();
		pricePane.setEditable(false);
		pricePane.setFont(new Font("Comic Sans MS", Font.BOLD, 22));
		pricePane.setText("0.0€");
		pricePane.setBounds(293, 509, 200, 37);
		contentPane.add(pricePane);
	}

	private void setInvoiceScroll() {
		invoiceScroll = new JScrollPane(invTextPane);
		invoiceScroll.setLocation(10, 202);
		invoiceScroll.setSize(776, 280);
		contentPane.add(invoiceScroll);
	}

	private void setOrderScroll() {
		orderScroll = new JScrollPane(currentOrderPane);
		orderScroll.setLocation(293, 130);
		orderScroll.setSize(493, 60);
		contentPane.add(orderScroll);
	}

	private void setPriceLabel() {
		priceLabel = new JLabel("Total Price:");
		priceLabel.setFont(new Font("Comic Sans MS", Font.BOLD, 22));
		priceLabel.setForeground(Color.ORANGE);
		priceLabel.setBounds(108, 508, 135, 30);
		contentPane.add(priceLabel);
	}

	private void setInvoiceLabel() {
		invoiceLabel = new JLabel("Table Invoice:");
		invoiceLabel.setFont(new Font("Comic Sans MS", Font.BOLD, 24));
		invoiceLabel.setForeground(Color.ORANGE);
		invoiceLabel.setBounds(20, 148, 200, 42);
		contentPane.add(invoiceLabel);
	}

	private void initializeCoffeeArray() {
		coffeeModel = new Coffee[2];
		coffeeModel[0] = new ArabicCoffee();
		coffeeModel[1] = new KopiLuwakCoffee();
	}

	private void initializeCoffeeDecArray() {
		coffeeDecModel = new CoffeeDecorator[2];
		Coffee base = new ArabicCoffee();
		coffeeDecModel[0] = new MilkDecorator(base);
		coffeeDecModel[1] = new ChocolateDecorator(base);
	}

	private void initializeCocktailArray() {
		cocktailModel = new Cocktail[2];
		cocktailModel[0] = new Martini();
		cocktailModel[1] = new FruitCocktail();
	}

	private void initializeCocktailDecArray() {
		cocktailDecModel = new CocktailDecorator[2];
		Cocktail base = new Martini();
		cocktailDecModel[0] = new AppetizerDecorator(base);
		cocktailDecModel[1] = new SodaDecorator(base);
	}

	public abstract void actionPerformed(ActionEvent e);
}
