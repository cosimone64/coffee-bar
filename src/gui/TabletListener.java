package gui;

import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import core.Cocktail;
import core.CocktailDecorator;
import core.Coffee;
import core.CoffeeDecorator;

public class TabletListener extends Tablet {
	private static final long serialVersionUID = 1L;

	public TabletListener() {
		addActionListeners();
		setActionCommands();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "coffeeBox":
			performCoffeeBoxAction();
			break;
		case "cocktailBox":
			performCocktailBoxAction();
			break;
		case "decoratorBox":
			performDecoratorBoxAction();
			break;
		case "orderButton":
			performOrderButtonAction();
			break;
		}
	}

	private void addActionListeners() {
		coffeeBox.addActionListener(this);
		cocktailBox.addActionListener(this);
		coffeeDecoratorBox.addActionListener(this);
		cocktailDecoratorBox.addActionListener(this);
		orderButton.addActionListener(this);
	}

	private void setActionCommands() {
		coffeeBox.setActionCommand("coffeeBox");
		cocktailBox.setActionCommand("cocktailBox");
		coffeeDecoratorBox.setActionCommand("decoratorBox");
		cocktailDecoratorBox.setActionCommand("decoratorBox");
		orderButton.setActionCommand("orderButton");
	}

	private void performCoffeeBoxAction() {
		cocktailDecoratorBox.setVisible(false);
		coffeeDecoratorBox.setVisible(true);
		int index = coffeeBox.getSelectedIndex();
		selectedBeverage = coffeeBox.getItemAt(index);
		currentOrderPane.setText("" + selectedBeverage);
	}

	private void performCocktailBoxAction() {
		coffeeDecoratorBox.setVisible(false);
		cocktailDecoratorBox.setVisible(true);
		int index = cocktailBox.getSelectedIndex();
		selectedBeverage = cocktailBox.getItemAt(index);
		currentOrderPane.setText("" + selectedBeverage);
	}

	private void performDecoratorBoxAction() {
		if (selectedBeverage instanceof Coffee)
			getDecoratedCoffee();
		if (selectedBeverage instanceof Cocktail)
			getDecoratedCocktail();
		String orderToAdd = selectedBeverage.getDescription();
		currentOrderPane.setText(orderToAdd);
	}

	private void getDecoratedCoffee() {
		int index = coffeeDecoratorBox.getSelectedIndex();
		CoffeeDecorator tempDec = coffeeDecoratorBox.getItemAt(index);
		Coffee coffeeToDecorate = (Coffee) selectedBeverage;
		selectedBeverage = tempDec.createCoffeeDec(coffeeToDecorate);
	}

	private void getDecoratedCocktail() {
		int index = cocktailDecoratorBox.getSelectedIndex();
		CocktailDecorator tempDec = cocktailDecoratorBox
				.getItemAt(index);
		Cocktail cocktailToDecorate = (Cocktail) selectedBeverage;
		selectedBeverage = tempDec
				.createCocktailDec(cocktailToDecorate);
	}

	private void performOrderButtonAction() {
		if (currentOrderPane.getText().equals("")) {
			String message = "Choose a beverage first!";
			JOptionPane.showMessageDialog(getParent(), message);
		} else {
			table.addOrder(selectedBeverage);
			updateInvoice();
		}
	}

	private void updateInvoice() {
		String description = table.getDescription();
		invTextPane.setText(description);
		pricePane.setText(table.getPrice() + "€");
		currentOrderPane.setText("");
	}
}
