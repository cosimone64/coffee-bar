package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import core.AppetizerDecorator;
import core.Cocktail;
import core.Invoice;
import core.Martini;
import core.Order;
import core.SimpleInvoice;
import exception.InvaldDeleteException;
import exception.InvalidInvoiceException;
import exception.InvalidOperationException;

public class TestAppetizerDecorator {
	private AppetizerDecorator appetizerDecorator;
	private Martini simpleCocktail;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Before
	public void setUp() {
		simpleCocktail = new Martini();
		appetizerDecorator = new AppetizerDecorator(simpleCocktail);
	}

	@Test
	public void testGetPrice() {
		double tmp = simpleCocktail.getPrice()
		        + appetizerDecorator.getAppetizerPrice();
		assertTrue(tmp == appetizerDecorator.getPrice());
	}

	@Test
	public void testGetDescription() {
		String tmp = simpleCocktail.getDescription()
		        + appetizerDecorator.getAppetizerDesc();
		assertEquals(tmp, appetizerDecorator.getDescription());
	}

	@Test
	public void testToString() {
		assertEquals("Appetizer", appetizerDecorator.toString());
	}

	@Test
	public void testCreatedType() {
		Cocktail c = appetizerDecorator.createCocktailDec(simpleCocktail);
		assertTrue(c instanceof AppetizerDecorator);
	}

	@Test(expected = InvalidOperationException.class)
	public void testAddOrder() throws InvalidOperationException {
		Order orderToAdd = new Martini();
		appetizerDecorator.addOrder(orderToAdd);
	}

	@Test(expected = InvaldDeleteException.class)
	public void testDeleteOrder() throws InvaldDeleteException {
		appetizerDecorator.deleteOrder(0);
	}

	@Test(expected = InvalidInvoiceException.class)
	public void testSetInvoice() throws InvalidInvoiceException {
		Invoice i = new SimpleInvoice();
		appetizerDecorator.setInvoice(i);
	}
}
