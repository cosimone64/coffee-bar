package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import core.SodaDecorator;
import core.Cocktail;
import core.Invoice;
import core.Martini;
import core.Order;
import core.SimpleInvoice;
import exception.InvaldDeleteException;
import exception.InvalidInvoiceException;
import exception.InvalidOperationException;

public class TestSodaDecorator {
	private SodaDecorator sodaDecorator;
	private Martini simpleCocktail;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Before
	public void setUp() {
		simpleCocktail = new Martini();
		sodaDecorator = new SodaDecorator(simpleCocktail);
	}

	@Test
	public void testGetPrice() {
		double tmp = simpleCocktail.getPrice() + sodaDecorator.getSodaPrice();
		assertTrue(tmp == sodaDecorator.getPrice());
	}

	@Test
	public void testGetDescription() {
		String tmp = simpleCocktail.getDescription()
		        + sodaDecorator.getSodaDesc();
		assertEquals(tmp, sodaDecorator.getDescription());
	}

	@Test
	public void testToString() {
		assertEquals("Soda", sodaDecorator.toString());
	}

	@Test
	public void testCreatedType() {
		Cocktail c = sodaDecorator.createCocktailDec(simpleCocktail);
		assertTrue(c instanceof SodaDecorator);
	}

	@Test(expected = InvalidOperationException.class)
	public void testAddOrder() throws InvalidOperationException {
		Order orderToAdd = new Martini();
		sodaDecorator.addOrder(orderToAdd);
	}

	@Test(expected = InvaldDeleteException.class)
	public void testDeleteOrder() throws InvaldDeleteException {
		sodaDecorator.deleteOrder(0);
	}

	@Test(expected = InvalidInvoiceException.class)
	public void testSetInvoice() throws InvalidInvoiceException {
		Invoice i = new SimpleInvoice();
		sodaDecorator.setInvoice(i);
	}
}
