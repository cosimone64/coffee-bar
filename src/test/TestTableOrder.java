package test;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import core.*;
import exception.InvalidOperationException;
import exception.InvaldDeleteException;

public class TestTableOrder extends TestCase {
	private TableOrder table;
	private Cocktail martini, fruit;

	@Before
	public void setUp() {
		table = new TableOrder();
		martini = new Martini();
		fruit = new FruitCocktail();
	}

	@Test
	public void testGetPrice() {
		table.addOrder(martini);
		table.addOrder(fruit);
		double tmpTot = martini.getPrice() + fruit.getPrice();
		assertEquals(tmpTot, table.getPrice());

	}

	@Test
	public void testGetDescriprtion() {
		table.addOrder(martini);
		table.addOrder(fruit);
		table.setInvoice(new SimpleInvoice());
		String tmpDesc = martini.getDescription() + "\n" + martini.getPrice()
		        + "€\n" + fruit.getDescription() + "\n" + fruit.getPrice()
		        + "€\n";
		assertEquals(tmpDesc, table.getDescription());
	}

	@Test
	public void testAddOrder() throws InvalidOperationException {
		table.addOrder(martini);
		table.addOrder(fruit);
		Cocktail tmpFruitCocktail = new FruitCocktail();
		double tmpTot = martini.getPrice() + fruit.getPrice()
		        + tmpFruitCocktail.getPrice();
		table.addOrder(tmpFruitCocktail);
		assertEquals(tmpTot, table.getPrice());

	}

	@Test
	public void testDecoratedDescription() {
		Coffee tmpKopiLuwakCoffee = new KopiLuwakCoffee();
		Coffee tmpDecorated = new MilkDecorator(tmpKopiLuwakCoffee);
		String s = tmpDecorated.getDescription();
		assertEquals(s, "Kopi Luwak Coffee with milk");
	}

	@Test
	public void testDescriptionWithDec() throws InvalidOperationException {
		Coffee tmpKopiLuwakCoffee = new KopiLuwakCoffee();
		Coffee tmpDecorated = new MilkDecorator(tmpKopiLuwakCoffee);
		TableOrder tmpTable = new TableOrder();
		tmpTable.setInvoice(new SimpleInvoice());
		tmpTable.addOrder(tmpDecorated);
		String expected = "Kopi Luwak Coffee with milk" + "\n" + "81.0€" + "\n";
		assertEquals(tmpTable.getDescription(), expected);
	}

	@Test
	public void testDeleteOrder() throws InvaldDeleteException {
		table.addOrder(martini);
		table.addOrder(fruit);
		table.deleteOrder(1);
		assertEquals(martini.getPrice(), table.getPrice());
	}
}
