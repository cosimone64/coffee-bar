package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import core.ArabicCoffee;
import core.ChocolateDecorator;
import core.Coffee;
import core.Invoice;
import core.Order;
import core.SimpleInvoice;
import exception.InvaldDeleteException;
import exception.InvalidInvoiceException;
import exception.InvalidOperationException;

public class TestChocolateDecorator {
	private ChocolateDecorator chocolateDecorator;
	private ArabicCoffee simpleCoffee;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Before
	public void setUp() {
		simpleCoffee = new ArabicCoffee();
		chocolateDecorator = new ChocolateDecorator(simpleCoffee);
	}

	@Test
	public void testGetPrice() {
		double tmp = simpleCoffee.getPrice()
		        + chocolateDecorator.getChocolatePrice();
		assertTrue(tmp == chocolateDecorator.getPrice());
	}

	@Test
	public void testGetDescription() {
		String tmp = simpleCoffee.getDescription()
		        + chocolateDecorator.getChocolateDesc();
		assertEquals(tmp, chocolateDecorator.getDescription());
	}

	@Test
	public void testToString() {
		assertEquals("Chocolate", chocolateDecorator.toString());
	}

	@Test
	public void testCreatedType() {
		Coffee c = chocolateDecorator.createCoffeeDec(simpleCoffee);
		assertTrue(c instanceof ChocolateDecorator);
	}

	@Test(expected = InvalidOperationException.class)
	public void testAddOrder() throws InvalidOperationException {
		Order orderToAdd = new ArabicCoffee();
		chocolateDecorator.addOrder(orderToAdd);
	}

	@Test(expected = InvaldDeleteException.class)
	public void testDeleteOrder() throws InvaldDeleteException {
		chocolateDecorator.deleteOrder(0);
	}

	@Test(expected = InvalidInvoiceException.class)
	public void testSetInvoice() throws InvalidInvoiceException {
		Invoice i = new SimpleInvoice();
		chocolateDecorator.setInvoice(i);
	}
}
