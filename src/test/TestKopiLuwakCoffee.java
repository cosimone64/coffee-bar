package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import core.KopiLuwakCoffee;
import core.Invoice;
import core.Order;
import core.SimpleInvoice;
import exception.InvaldDeleteException;
import exception.InvalidInvoiceException;
import exception.InvalidOperationException;

public class TestKopiLuwakCoffee {
	private KopiLuwakCoffee kCoffee;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Before
	public void setUp() { kCoffee = new KopiLuwakCoffee(); }

	@Test
	public void testGetPrice() { assertTrue(kCoffee.getPrice() == 80); }

	@Test
	public void testGetDescription() {
		assertEquals(kCoffee.getDescription(), "Kopi Luwak Coffee");
	}

	@Test(expected = InvalidOperationException.class)
	public void testAddOrder() throws InvalidOperationException {
		Order orderToAdd = new KopiLuwakCoffee();
		kCoffee.addOrder(orderToAdd);
	}

	@Test(expected = InvaldDeleteException.class)
	public void testDeleteOrder() throws InvaldDeleteException {
		kCoffee.deleteOrder(0);
	}

	@Test(expected = InvalidInvoiceException.class)
	public void testSetInvoice() throws InvalidInvoiceException {
		Invoice i = new SimpleInvoice();
		kCoffee.setInvoice(i);
	}
}
