package test;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import core.AppetizerDecorator;
import core.ArabicCoffee;
import core.ChocolateDecorator;
import core.Invoice;
import core.Martini;
import core.Order;
import core.SimpleInvoice;
import core.TableOrder;

public class TestSimpleInvoice {
	private Invoice invoice;
	private TableOrder table;

	@Before
	public void setUp() {
		invoice = new SimpleInvoice();
		table = new TableOrder();
	}

	@Test
	public void testPrintInvoice() {
		Order tmp = new ChocolateDecorator(new ArabicCoffee());
		Order tmp2 = new AppetizerDecorator(new Martini());
		String desc = tmp.getDescription() + "\n" + tmp.getPrice() + "€\n";
		String desc2 = tmp2.getDescription() + "\n" + tmp2.getPrice() + "€\n";
		String totalDesc = desc + desc2;
		table.addOrder(tmp);
		table.addOrder(tmp2);
		table.setInvoice(invoice);
		assertEquals(totalDesc, invoice.printInvoice(table));
	}
}
