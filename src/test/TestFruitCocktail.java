package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import core.Invoice;
import core.FruitCocktail;
import core.Order;
import core.SimpleInvoice;
import exception.InvaldDeleteException;
import exception.InvalidInvoiceException;
import exception.InvalidOperationException;

public class TestFruitCocktail {
	private FruitCocktail fruitCocktail;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Before
	public void setUp() { fruitCocktail = new FruitCocktail(); }

	@Test
	public void testGetPrice() {
		assertTrue(fruitCocktail.getPrice() == 4);
	}

	@Test
	public void testGetDescription() {
		assertEquals(fruitCocktail.getDescription(), "Fruit Cocktail");
	}

	@Test(expected = InvalidOperationException.class)
	public void testAddOrder() throws InvalidOperationException {
		Order orderToAdd = new FruitCocktail();
		fruitCocktail.addOrder(orderToAdd);
	}

	@Test(expected = InvaldDeleteException.class)
	public void testDeleteOrder() throws InvaldDeleteException {
		fruitCocktail.deleteOrder(0);
	}

	@Test(expected = InvalidInvoiceException.class)
	public void testSetInvoice() throws InvalidInvoiceException {
		Invoice i = new SimpleInvoice();
		fruitCocktail.setInvoice(i);
	}
}
