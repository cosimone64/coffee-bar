package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import core.ArabicCoffee;
import core.Coffee;
import core.Invoice;
import core.MilkDecorator;
import core.Order;
import core.SimpleInvoice;
import exception.InvaldDeleteException;
import exception.InvalidInvoiceException;
import exception.InvalidOperationException;

public class TestMilkDecorator {
	private MilkDecorator milkDecorator;
	private Coffee simpleCoffee;

	@Before
	public void setUp() {
		simpleCoffee = new ArabicCoffee();
		milkDecorator = new MilkDecorator(simpleCoffee);
	}

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Test
	public void testGetPrice() {
		double tmp = simpleCoffee.getPrice() + milkDecorator.getMilkPrice();
		assertTrue(tmp == milkDecorator.getPrice());
	}

	@Test
	public void testGetDescription() {
		String tmp = simpleCoffee.getDescription()
		        + milkDecorator.getMilkDesc();
		assertEquals(tmp, milkDecorator.getDescription());
	}

	@Test
	public void testToString() {
		assertEquals("Milk", milkDecorator.toString());
	}

	@Test
	public void testCreatedType() {
		Coffee c = milkDecorator.createCoffeeDec(simpleCoffee);
		assertTrue(c instanceof MilkDecorator);
	}

	@Test(expected = InvalidOperationException.class)
	public void testAddOrder() throws InvalidOperationException {
		Order orderToAdd = new ArabicCoffee();
		milkDecorator.addOrder(orderToAdd);
	}

	@Test(expected = InvaldDeleteException.class)
	public void testDeleteOrder() throws InvaldDeleteException {
		milkDecorator.deleteOrder(0);
	}

	@Test(expected = InvalidInvoiceException.class)
	public void testSetInvoice() throws InvalidInvoiceException {
		Invoice i = new SimpleInvoice();
		milkDecorator.setInvoice(i);
	}
}
