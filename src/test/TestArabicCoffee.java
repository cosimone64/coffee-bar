package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import core.Invoice;
import core.ArabicCoffee;
import core.Order;
import core.SimpleInvoice;
import exception.InvaldDeleteException;
import exception.InvalidInvoiceException;
import exception.InvalidOperationException;

public class TestArabicCoffee {
	private ArabicCoffee aCoffee;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Before
	public void setUp() { aCoffee = new ArabicCoffee(); }

	@Test
	public void testGetPrice() { assertTrue(aCoffee.getPrice() == 2); }

	@Test
	public void testGetDescription() {
		assertEquals(aCoffee.getDescription(), "Arabic Coffee");
	}

	@Test(expected = InvalidOperationException.class)
	public void testAddOrder() throws InvalidOperationException {
		Order orderToAdd = new ArabicCoffee();
		aCoffee.addOrder(orderToAdd);
	}

	@Test(expected = InvaldDeleteException.class)
	public void testDeleteOrder() throws InvaldDeleteException {
		aCoffee.deleteOrder(0);
	}

	@Test(expected = InvalidInvoiceException.class)
	public void testSetInvoice() throws InvalidInvoiceException {
		Invoice i = new SimpleInvoice();
		aCoffee.setInvoice(i);
	}
}
