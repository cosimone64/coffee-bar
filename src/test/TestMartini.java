package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import core.Invoice;
import core.Martini;
import core.Order;
import core.SimpleInvoice;
import exception.InvaldDeleteException;
import exception.InvalidInvoiceException;
import exception.InvalidOperationException;

public class TestMartini {
	private Martini martini;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Before
	public void setUp() { martini = new Martini(); }

	@Test
	public void testGetPrice() { assertTrue(martini.getPrice() == 6); }

	@Test
	public void testGetDescription() {
		assertEquals(martini.getDescription(), "Martini");
	}

	@Test(expected = InvalidOperationException.class)
	public void testAddOrder() throws InvalidOperationException {
		Order orderToAdd = new Martini();
		martini.addOrder(orderToAdd);
	}

	@Test(expected = InvaldDeleteException.class)
	public void testDeleteOrder() throws InvaldDeleteException {
		martini.deleteOrder(0);
	}

	@Test(expected = InvalidInvoiceException.class)
	public void testSetInvoice() throws InvalidInvoiceException {
		Invoice i = new SimpleInvoice();
		martini.setInvoice(i);
	}
}
